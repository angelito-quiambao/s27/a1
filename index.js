const HTTP = require('http');
const PORT = 4000;

let courses = [
    {
        "courseName": "HTML",
        "availableSlot": "10"
    },
    {
        "courseName": "CSS",
        "availableSlot": "10"
    },
    {
        "courseName": "Javascript",
        "availableSlot": "15"
    }
]


HTTP.createServer((req, res) => {
    if(req.url == "/" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to Booking System.")
    }
    else if(req.url == "/profile" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to your profile!")
    }
    else if(req.url == "/courses" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(`Here's our courses available\n ${JSON.stringify(courses)}`)
        res.end()
    }
    else if(req.url == "/addCourse" && req.method == "POST"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write("Add course to our resources\n")

        let reqBody = "";

        req.on("data", (data) => {
            reqBody += data
        })

        req.on("end", () => {
            reqBody = JSON.parse(reqBody)
            courses.push(reqBody)
            res.write(JSON.stringify(courses))
            res.end()
        })
    }
    else if(req.url == "/updateCourse" && req.method == "PUT"){
         res.writeHead(200, {"Content-Type": "text/plain"})
         res.write("Update a course to our resources\n")

         let reqBody = "";

        req.on("data", (data) => {
            reqBody += data
        })

        req.on("end", () => {
            reqBody = JSON.parse(reqBody)
           // console.log(reqBody.courseName)

        courses.forEach(element => {
            if(element.courseName === reqBody.courseName){
                element.availableSlot = reqBody.availableSlot
            }
        })

           res.write(JSON.stringify(courses))
           res.end()
        })
    }
    else if(req.url == "/archiveCourse" && req.method == "DELETE"){
         res.writeHead(200, {"Content-Type": "text/plain"})
         res.write("Archive courses to our resources\n")

         let reqBody = "";

        req.on("data", (data) => {
            reqBody += data
        })

        req.on("end", () => {
            reqBody = JSON.parse(reqBody)
          //  console.log(reqBody.courseName)

            let objIndex = ""

           for(let x = 0; x < courses.length; x++){
                if(reqBody.courseName  === courses[x].courseName){
                    objIndex = x
                }
           }

           //delete course
           courses.splice(objIndex, 1);
           //console.log(objIndex)

           res.write(JSON.stringify(courses))
           res.end()
        })
         
    }
    else{
        res.writeHead(404, {"Content-Type": "text/plain"})
        res.write("Page not found!")
    }

}).listen(PORT, () => console.log(`Server connected to ${PORT}`))